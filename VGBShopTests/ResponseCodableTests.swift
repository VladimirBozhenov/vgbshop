//
//  ResponseCodableTests.swift
//  VGBShopTests
//
//  Created by Vladimir Bozhenov on 06/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest
import Alamofire
@testable import VGBShop

struct PostStub: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}

enum ApiErrorStub: Error {
    case fatalError
}

struct ErrorParserStub: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return ApiErrorStub.fatalError
    }

    func parse(response: HTTPURLResponse?,
               data: Data?,
               error: Error?) -> Error? {
        return error
    }
}

class ResponseCodableTests: XCTestCase {
    let expectation = XCTestExpectation(description: "Download https://jsonplaceholder.typicode.com/posts/1")
    let expectionTimeout: TimeInterval = 2
    var errorParser: ErrorParserStub!

    override func setUp() {
        super.setUp()
        errorParser = ErrorParserStub()
    }

    override func tearDown() {
        super.tearDown()
        errorParser = nil
    }

    func testShouldDownloadAndParse() {
        Alamofire
            .request("https://jsonplaceholder.typicode.com/posts/1")
            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<PostStub>) in
                switch response.result {
                case .success: break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation],
             timeout: expectionTimeout)
    }

}
