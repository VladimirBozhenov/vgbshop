//
//  RequestsTest.swift
//  VGBShopTests
//
//  Created by Vladimir Bozhenov on 06/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest
@testable import VGBShop

class RequestsTest: XCTestCase {

    let requestFactory = RequestFactory()
    let expectionTimeout: TimeInterval = 2

    let userData = RegistrationParams(
        userID: 123,
        username: "Somebody",
        password: "mypassword",
        email: "some@some.ru",
        gender: "m",
        creditCardNumber: "9872389-2424-234224-234",
        bio: "This is good!")

    let pageNumber = 1
    let categoryID = 1
    let productID = 123

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testLogin() {

        let loginExpectation = XCTestExpectation(description: "Login successfull")
        let auth = requestFactory.makeAuthRequestFactory()
        auth.login(userName: userData.username,
                   password: userData.password) { response in
                    switch response.result {
                    case .success(let login):
                        print("Login: \(login)")
                        loginExpectation.fulfill()
                    case .failure:
                    break
                }
        }

        wait(for: [loginExpectation],
             timeout: expectionTimeout)
    }

    func testRegistration() {

        let registrationExpectation = XCTestExpectation(description: "Registration successfull")
        let registration = requestFactory.registrationUserRequestFactory()
        registration.registerUser(params: userData) { response in
            switch response.result {
            case .success(let registration):
                print("Registration: \(registration)")
                registrationExpectation.fulfill()
            case .failure:
                break
            }
        }

        wait(for: [registrationExpectation],
             timeout: expectionTimeout)
    }

    func testLogout() {

        let logoutExpectation = XCTestExpectation(description: "Logout successfull")
        let logout = requestFactory.makeLogoutRequestFactory()
        logout.logout(userId: 123) { response in
            switch response.result {
            case .success(let logout):
                print("Logout: \(logout)")
                logoutExpectation.fulfill()
            case .failure:
                break
            }
        }

        wait(for: [logoutExpectation],
             timeout: expectionTimeout)
    }

    func testUserDataChange() {

        let changeUserDataExpectation = XCTestExpectation(description: "User data changed successfull")
        let changeUserData = requestFactory.makeChangeUserDataRequestFactory()
        changeUserData.changeUserData(params: userData) { response in
            switch response.result {
            case .success(let change):
                print("User data changed: \(change)")
                changeUserDataExpectation.fulfill()
            case .failure:
                break
            }
        }

        wait(for: [changeUserDataExpectation],
             timeout: expectionTimeout)
    }

    func testGetProductList() {

        let getProductListExpectation = XCTestExpectation(description: "Product list was got successfull")
        let getProductList = requestFactory.makeProductListRequestFactory()
        getProductList.getProductList(pageNumber: pageNumber,
                                      categoryID: categoryID ) { response in
                                        switch response.result {
                                        case .success(let productList):
                                            print("Product list was got: \(productList)")
                                            getProductListExpectation.fulfill()
                                        case .failure:
                break
            }
        }

        wait(for: [getProductListExpectation],
             timeout: expectionTimeout)
    }

    func testGetProduct() {

        let getProductExpectation = XCTestExpectation(description: "Product was got successfull")
        let getProduct = requestFactory.makeProductRequestFactory()
        getProduct.getProduct(productID: productID ) { response in
            switch response.result {
            case .success(let product):
                print("Product was got: \(product)")
                getProductExpectation.fulfill()
            case .failure:
                break
            }
        }

        wait(for: [getProductExpectation],
             timeout: expectionTimeout)
    }

}
