//
//  BasketRequestFactory.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Alamofire

protocol BasketRequestFactory {
    func addToBasket(productID: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void)
    func deleteFromBasket(productID: Int,
                          completionHandler: @escaping (DataResponse<DeleteFromBasketResult>) -> Void)
    func getBasket(userID: Int,
                   completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void)
}
