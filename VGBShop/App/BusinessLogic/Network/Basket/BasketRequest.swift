//
//  BasketRequest.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Alamofire

class BasketRequest: BaseRequestFactory, BasketRequestFactory {
    func addToBasket(productID: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void) {
        let addToBasketRequest = AddToBasketRequest(baseUrl: baseUrl,
                                                    productID: productID,
                                                    quantity: quantity)
        request(request: addToBasketRequest,
                completionHandler: completionHandler)
    }
    
    func deleteFromBasket(productID: Int,
                          completionHandler: @escaping (DataResponse<DeleteFromBasketResult>) -> Void) {
        let deleteFromBasketRequest = DeleteFromBasketRequest(baseUrl: baseUrl,
                                                              productID: productID)
        request(request: deleteFromBasketRequest,
                completionHandler: completionHandler)
    }
    
    func getBasket(userID: Int,
                   completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void) {
        let getBasketRequest = GetBasketRequest(baseUrl: baseUrl,
                                                userID: userID)
        request(request: getBasketRequest,
                completionHandler: completionHandler)
    }
}

extension BasketRequest {
    struct AddToBasketRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "addToBasket"
        let productID: Int
        let quantity: Int
        
        var parameters: Parameters? {
            return [
                "id_product": productID,
                "quantity": quantity
            ]
        }
    }
}

extension BasketRequest {
    struct DeleteFromBasketRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "deleteFromBasket"
        let productID: Int
        
        var parameters: Parameters? {
            return [
                "id_product": productID
            ]
        }
    }
}

extension BasketRequest {
    struct GetBasketRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "getBasket"
        let userID: Int
        
        var parameters: Parameters? {
            return [
                "id_user": userID
            ]
        }
    }
}

