//
//  ProductRequestFactory.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 05/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import Alamofire

protocol ProductRequestFactory {
    func getProductList(pageNumber: Int,
                        categoryID: Int,
                        completionHandler: @escaping (DataResponse<ProductListResult>) -> Void)
    func getProduct(productID: Int,
                    completionHandler: @escaping (DataResponse<ProductResult>) -> Void)
}
