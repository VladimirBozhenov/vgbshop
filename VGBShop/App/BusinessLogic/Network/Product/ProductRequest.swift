//
//  ProductRequest.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 05/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import Alamofire

class ProductRequest: BaseRequestFactory, ProductRequestFactory {
    
    func getProductList(pageNumber: Int,
                        categoryID: Int,
                        completionHandler: @escaping (DataResponse<ProductListResult>) -> Void) {
        let getProductsRequest = ProductList(baseUrl: baseUrl,
                                             pageNumber: pageNumber,
                                             categoryID: categoryID)
        self.request(request: getProductsRequest,
                     completionHandler: completionHandler)
    }
    
    func getProduct(productID: Int,
                    completionHandler: @escaping (DataResponse<ProductResult>) -> Void) {
        let getProductRequest = Product(baseUrl: baseUrl,
                                        productID: productID)
        self.request(request: getProductRequest,
                     completionHandler: completionHandler)
    }
}

extension ProductRequest {
    struct ProductList: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "catalogData"
        let pageNumber: Int
        let categoryID: Int
        
        var parameters: Parameters? {
            return [
                "page_number": pageNumber,
                "id_category": categoryID
            ]
        }
    }
}

extension ProductRequest {
    struct Product: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "getGoodById"
        let productID: Int
        
        var parameters: Parameters? {
            return [
                "id_product": productID
            ]
        }
    }
}
