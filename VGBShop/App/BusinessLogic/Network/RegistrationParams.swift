//
//  RegistrationParams.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 03/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
struct RegistrationParams {
    var userID: Int
    var username: String
    var password: String
    var email: String
    var gender: String
    var creditCardNumber: String
    var bio: String
}
