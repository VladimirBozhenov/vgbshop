//
//  AuthRequestFactory.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 02/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthRequestFactory {
    func login(userName: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
    func registerUser(params: RegistrationParams,
                      completionHandler: @escaping (DataResponse<RegistrationResult>) -> Void)
    func logout(userId: Int,
                completionHandler: @escaping (DataResponse<LogoutResult>) -> Void)
    func changeUserData(params: RegistrationParams,
                        completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void)
}
