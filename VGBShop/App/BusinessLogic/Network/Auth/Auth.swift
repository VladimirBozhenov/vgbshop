//
//  Auth.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 02/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import Alamofire

class Auth: BaseRequestFactory, AuthRequestFactory {
    func login(userName: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void) {
        let requestModel = Login(baseUrl: baseUrl,
                                 login: userName,
                                 password: password)
        self.request(request: requestModel,
                     completionHandler: completionHandler)
    }
    
    func registerUser(params: RegistrationParams,
                      completionHandler: @escaping (DataResponse<RegistrationResult>) -> Void) {
        let changeUserDataRequest = RegisterUser(baseUrl: baseUrl,
                                                 changeUserDataParams: params)
        request(request: changeUserDataRequest,
                completionHandler: completionHandler)
    }
    
    func logout(userId: Int,
                completionHandler: @escaping (DataResponse<LogoutResult>) -> Void) {
        let requestModel = Logout(baseUrl: baseUrl,
                                  userID: userId)
        self.request(request: requestModel,
                     completionHandler: completionHandler)
    }
    
    func changeUserData(params: RegistrationParams,
                        completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void) {
        let changeUserDataRequest = ChangeUserData(baseUrl: baseUrl,
                                                   changeUserDataParams: params)
        request(request: changeUserDataRequest,
                completionHandler: completionHandler)
    }
}

extension Auth {
    struct Login: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "login"
        
        let login: String
        let password: String
        var parameters: Parameters? {
            return [
                "username": login,
                "password": password
            ]
        }
    }
}

extension Auth {
    struct RegisterUser: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "registerUser"
        let changeUserDataParams: RegistrationParams
        
        var parameters: Parameters? {
            return [
                "id_user": changeUserDataParams.userID,
                "username": changeUserDataParams.username,
                "password": changeUserDataParams.password,
                "email": changeUserDataParams.email,
                "gender": changeUserDataParams.gender,
                "credit_card": changeUserDataParams.creditCardNumber,
                "bio": changeUserDataParams.bio
            ]
        }
    }
}

extension Auth {
    struct Logout: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "logout"
        let userID: Int
        
        var parameters: Parameters? {
            return [
                "id_user": userID
            ]
        }
    }
}

extension Auth {
    struct ChangeUserData: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "changeUserData"
        let changeUserDataParams: RegistrationParams
        
        var parameters: Parameters? {
            return [
                "id_user": changeUserDataParams.userID,
                "username": changeUserDataParams.username,
                "password": changeUserDataParams.password,
                "email": changeUserDataParams.email,
                "gender": changeUserDataParams.gender,
                "credit_card": changeUserDataParams.creditCardNumber,
                "bio": changeUserDataParams.bio
            ]
        }
    }
}

