//
//  ReviewRequest.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import Alamofire

class ReviewRequest: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://localhost:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension ReviewRequest: ReviewRequestFactory {
    func addReview(userID: Int,
                   text: String,
                   completionHandler: @escaping (DataResponse<AddReviewResult>) -> Void) {
        let addReviewRequest = AddReviewRequest(baseUrl: baseUrl,
                                                userID: userID,
                                                text: text)
        request(request: addReviewRequest,
                completionHandler: completionHandler)
    }
    func approveReview(commentID: Int,
                       completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void) {
        let approveReviewRequest = ApproveReviewRequest(baseUrl: baseUrl,
                                                        commentID: commentID)
        request(request: approveReviewRequest,
                completionHandler: completionHandler)
    }

    func removeReview(commentID: Int,
                      completionHandler: @escaping (DataResponse<RemoveReviewResult>) -> Void) {
        let removeReviewRequest = RemoveReviewRequest(baseUrl: baseUrl,
                                                      commentID: commentID)
        request(request: removeReviewRequest,
                completionHandler: completionHandler)
    }
}

extension ReviewRequest {
    struct AddReviewRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "addReview"
        let userID: Int
        let text: String

        var parameters: Parameters? {
            return [
                "id_user": userID,
                "text": text
            ]
        }
    }
}

extension ReviewRequest {
    struct ApproveReviewRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "approveReview"
        let commentID: Int

        var parameters: Parameters? {
            return [
                "id_comment": commentID
            ]
        }
    }
}

extension ReviewRequest {
    struct RemoveReviewRequest: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "removeReview"
        let commentID: Int

        var parameters: Parameters? {
            return [
                "id_comment": commentID
            ]
        }
    }
}
