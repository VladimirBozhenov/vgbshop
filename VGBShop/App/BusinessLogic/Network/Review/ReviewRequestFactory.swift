//
//  ReviewRequestFactory.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Alamofire

protocol ReviewRequestFactory {
    func addReview(userID: Int, text: String,
                   completionHandler: @escaping (DataResponse<AddReviewResult>) -> Void)
    func approveReview(commentID: Int,
                       completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void)
    func removeReview(commentID: Int,
                      completionHandler: @escaping (DataResponse<RemoveReviewResult>) -> Void)
}
