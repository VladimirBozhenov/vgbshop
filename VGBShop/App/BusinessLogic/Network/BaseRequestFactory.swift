//
//  BaseRequestFactory.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Alamofire

class BaseRequestFactory: AbstractRequestFactory {
    
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://localhost:8080/")!
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

