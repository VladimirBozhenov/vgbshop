//
//  AddReviewResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct AddReviewResult: Codable {
    let result: Int
    let userMessage: String
}
