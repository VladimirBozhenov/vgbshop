//
//  ApproveReviewResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 14/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct ApproveReviewResult: Codable {
    let result: Int
}
