//
//  UserBasket.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct UserBasket: Codable {
    let id: Int
    let name: String
    let price: Decimal
    let quantity: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "id_product"
        case name = "product_name"
        case price = "price"
        case quantity = "quantity"
    }
}
