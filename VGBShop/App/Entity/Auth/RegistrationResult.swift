//
//  RegistrationResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 02/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct RegistrationResult: Codable {
    let result: Int
    let userMessage: String
}
