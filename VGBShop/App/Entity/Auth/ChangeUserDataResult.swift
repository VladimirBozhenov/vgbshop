//
//  ChangeUserDataResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 02/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct ChangeUserDataResult: Codable {
    let result: Int
    let userMessage: String
}
