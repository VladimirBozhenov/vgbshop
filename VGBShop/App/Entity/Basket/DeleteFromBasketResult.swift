//
//  DeleteFromBasketResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct DeleteFromBasketResult: Codable {
    let result: Int
}
