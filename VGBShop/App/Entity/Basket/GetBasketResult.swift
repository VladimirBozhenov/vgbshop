//
//  GetBasketResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 18/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct GetBasketResult: Codable {
    let amount: Int
    let countGoods: Int
    let contents: [UserBasket]
}
