//
//  ProductListResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 05/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct ProductListResult: Codable {
    let products: [Product]
}
