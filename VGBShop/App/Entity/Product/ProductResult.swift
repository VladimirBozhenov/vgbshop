//
//  ProductResult.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 05/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

struct ProductResult: Codable {
    let result: Int
    let name: String
    let price: Decimal
    let description: String

    enum CodingKeys: String, CodingKey {
        case result
        case name = "product_name"
        case price = "product_price"
        case description = "product_description"
    }
}
