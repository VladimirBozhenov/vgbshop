//
//  ProductViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 28/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class ProductViewModel {
    
    private let model = ProductModel()
    var item: ProductResult!
    
    var itemsChanged: (() -> ())?
    var itemAddedToBasket: (() -> ())?
    
    func handleViewReady(productID: Int) {
        model.getData(productID: productID) { result in
                        guard let result = result else { return }
                        self.item = result
                        self.itemsChanged?()
        }
    }
    
    func addToBasket() {
        model.getData(productID: 123,
                      quantity: 1) { result in
                        self.itemAddedToBasket?()
        }
    }
    
    func productPrice() -> String {
        return "\(item.price) руб."
    }
    func productDescription() -> String {
        return item.description
    }
}
