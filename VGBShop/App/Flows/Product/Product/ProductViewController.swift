//
//  ProductViewController.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 27/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    let viewModel = ProductViewModel()
    let alert = Alert()
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var descript: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.itemsChanged = { [unowned self] in
            DispatchQueue.main.async {
                self.price.text = self.viewModel.productPrice()
                self.descript.text = self.viewModel.productDescription()
            }
        }
    }
    
    @IBAction func addToBasketButton(_ sender: Any) {
        viewModel.addToBasket()
        viewModel.itemAddedToBasket = { [unowned self] in
            DispatchQueue.main.async {
                self.alert.showAlertWith(title: "Спасибо за покупку",
                                         message: "Товар успешно добавлен в корзину.",
                                         in: self)
            }
        }
    }
}
