//
//  ProductModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 27/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class ProductModel {
    
    private let requestFactory = RequestFactory()
    
    func getData(productID: Int,
                 completion: @escaping (ProductResult?) -> ()) {
        let product = requestFactory.makeProductRequestFactory()
        product.getProduct(productID: productID) { response in
            switch response.result {
            case .success(let product):
                completion(product)
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
    
    func getData(productID: Int,
                 quantity: Int,
                 completion: @escaping (AddToBasketResult?) -> ()) {
        let basket = requestFactory.makeBasketRequestFactory()
        basket.addToBasket(productID: productID,
                           quantity: quantity) { response in
                            switch response.result {
                            case .success(let basket):
                                completion(basket)
                            case .failure(let error):
                                print(error.localizedDescription)
                                completion(nil)
                            }
        }
    }
}
