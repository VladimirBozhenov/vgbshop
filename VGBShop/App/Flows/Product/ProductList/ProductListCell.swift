//
//  ProductListCell.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 23/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {
    
    var product: Product!

    //MARK: - IBOutlets -
    @IBOutlet weak var productIdLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    func setupCell(with product: Product) {
        productIdLabel.text = String(product.id)
        productNameLabel.text = product.name
        productPriceLabel.text = "\(product.price) руб."
    }
}
