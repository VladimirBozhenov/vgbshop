//
//  ProductListViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 28/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class ProductListViewModel {
    private let model = ProductListModel()
    var items: [Product] = []
    
    var numberOfItems: Int {
        return items.count
    }
    var itemsChanged: (() -> ())?

    func handleViewReady() {
        model.getData(pageNumber: 1,
                      categoryID: 1) { result in
                        guard let result = result else { return }
                        self.items = result.products
                        self.itemsChanged?()
        }
    }
    
    func productIdForIndex(_ index: Int) -> String {
        return "\(items[index].id)"
    }
    func productNameForIndex(_ index: Int) -> String {
        return items[index].name
    }
    func productPriceForIndex(_ index: Int) -> String {
        return "\(items[index].price) руб."
    }
}
