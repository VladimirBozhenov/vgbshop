//
//  ProductListModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 27/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class ProductListModel {
    
    private let requestFactory = RequestFactory()
    
    func getData(pageNumber: Int,
                 categoryID: Int,
                 completion: @escaping (ProductListResult?) -> ()) {
        let productList = requestFactory.makeProductListRequestFactory()
        productList.getProductList(pageNumber: pageNumber,
                                   categoryID: categoryID) { response in
                                    switch response.result {
                                    case .success(let productList):
                                        completion(productList)
                                    case .failure(let error):
                                        print(error.localizedDescription)
                                        completion(nil)
                                    }
        }
    }
}
