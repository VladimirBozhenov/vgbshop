//
//  ProductListViewController.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 23/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    
    //MARK: - Properties -
    private let viewModel = ProductListViewModel()
    
    @IBOutlet weak var productListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Список товаров"
        
        viewModel.handleViewReady()
        viewModel.itemsChanged = { [unowned self] in
            DispatchQueue.main.async {
                self.productListTableView.reloadData()
            }
        }
    }
    
    //MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        if segue.identifier == "ShowDetails" {
            let productVC = segue.destination as! ProductViewController
            if let indexPath = self.productListTableView.indexPathForSelectedRow {
                productVC.viewModel.handleViewReady(productID: Int(viewModel.productIdForIndex(indexPath.row))!)
                productVC.title = viewModel.productNameForIndex(indexPath.row)
            }
        }
    }
}

extension ProductListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell",
                                                 for: indexPath) as! ProductListCell
        cell.setupCell(with: viewModel.items[indexPath.row])
        return cell
    }
}
