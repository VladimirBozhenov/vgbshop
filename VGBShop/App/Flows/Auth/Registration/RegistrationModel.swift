//
//  RegistrationModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 27/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class RegistrationModel {
    
    private let requestFactory = RequestFactory()
    
    func getData(userName: String,
                 password: String,
                 email: String,
                 gender: String,
                 creditCardNumber: String,
                 bio: String,
                 completion: @escaping (RegistrationResult?) -> ()) {
        let registerUser = requestFactory.registrationUserRequestFactory()
        registerUser.registerUser(params: RegistrationParams(userID: 123,
                                                             username: userName,
                                                             password: password,
                                                             email: email,
                                                             gender: gender,
                                                             creditCardNumber: creditCardNumber,
                                                             bio: bio)) { response in
                                                                switch response.result {
                                                                case .success(let registerUser):
                                                                    completion(registerUser)
                                                                case .failure(let error):
                                                                    print(error.localizedDescription)
                                                                    completion(nil)
                                                                }
        }
    }
}
