//
//  RegistrationViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 28/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class RegistrationViewModel {
    
    private let model = RegistrationModel()
    var item: RegistrationResult!
    
    var itemsChanged: (() -> ())?
    var error: (() -> ())?
    
    func handleViewReady(userName: String,
                         password: String,
                         email: String,
                         gender: String,
                         creditCardNumber: String,
                         bio: String) {
        model.getData(userName: userName,
                      password: password,
                      email: email,
                      gender: gender,
                      creditCardNumber: creditCardNumber,
                      bio: bio) { result in
                        if let result = result {
                            self.item = result
                            self.itemsChanged?()
                        } else {
                            self.error?()
                        }
        }
    }
}
