//
//  RegistrationViewController.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 21/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    //MARK: - Properties -
    private let viewModel = RegistrationViewModel()
    private let alert = Alert()

    //MARK: - IBOutlets -
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var creditCardTextField: UITextField!
    @IBOutlet weak var bioTextField: UITextField!
    
    //MARK: - LifeCicle -
    override func viewDidLoad() {
        super.viewDidLoad()
        let hideKeyboardGesture = UITapGestureRecognizer(target: self,
                                                         action: #selector(hideKeyboard))
        scrollView?.addGestureRecognizer(hideKeyboardGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWasShown),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillHideNotification,
                                                  object: nil)
    }
    
    //MARK: - IBActions -
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        guard let userName = nameTextField.text else { return }
        guard let password = passTextField.text else { return }
        guard let email = emailTextField.text else { return }
        guard let gender = genderTextField.text else { return }
        guard let creditCardNumber = creditCardTextField.text else { return }
        guard let bio = bioTextField.text else { return }
        
        viewModel.handleViewReady(userName: userName,
                                  password: password,
                                  email: email,
                                  gender: gender,
                                  creditCardNumber: creditCardNumber,
                                  bio: bio)
        viewModel.itemsChanged = { [unowned self] in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "Login",
                                  sender: self)
            }
        }
        viewModel.error = { [unowned self] in
            DispatchQueue.main.async {
                self.alert.showAlertWith(title: "Ошибка",
                                         message: "Неверные данные для регистрации",
                                         in: self)
            }
        }
    }
    
    //MARK: - Keyboard methods -
    @objc func keyboardWasShown(notification: Notification) {
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0.0,
                                         left: 0.0,
                                         bottom: kbSize.height,
                                         right: 0.0)
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillBeHidden(notification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
    }
    
    @objc func hideKeyboard() {
        scrollView?.endEditing(true)
    }
    
    //MARK: - Navigation -
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        if segue.identifier == "Login" {
            //TODO: -Передадим данные-
        }
    }
}
