//
//  LoginModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 27/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class LoginModel {
    
    private let requestFactory = RequestFactory()

    func getData(userName: String,
                 password: String,
                 completion: @escaping (LoginResult?) -> ()) {
        let auth = requestFactory.makeAuthRequestFactory()
        auth.login(userName: userName,
                   password: password) { response in
                    switch response.result {
                    case .success(let login):
                        completion(login)
                    case .failure(let error):
                        completion(nil)
                        print(error.localizedDescription)
                    }
        }
    }
}
