//
//  LoginViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 28/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    private let model = LoginModel()
    var item: LoginResult!
    
    var itemsChanged: (() -> ())?
    var error: (() -> ())?
    
    func handleViewReady(userName: String,
                         password: String) {
        model.getData(userName: userName,
                      password: password) { result in
                        if let result = result {
                            self.item = result
                            self.itemsChanged?()
                        } else {
                            self.error?()
                        }
        }
    }
}
