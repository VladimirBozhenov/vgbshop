//
//  ChangeUserDataViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 28/08/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class ChangeUserDataViewModel {
    
    private let model = ChangeUserDataModel()
    var item: ChangeUserDataResult!
    
    var itemsChanged: (() -> ())?
    
    func handleViewReady(userName: String,
                         password: String,
                         email: String,
                         gender: String,
                         creditCardNumber: String,
                         bio: String) {
        model.getData(userName: userName,
                      password: password,
                      email: email,
                      gender: gender,
                      creditCardNumber: creditCardNumber,
                      bio: bio) { result in
                        guard let result = result else { return }
                        self.item = result
                        self.itemsChanged?()
        }
    }
}
