//
//  BasketTableViewCell.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 04/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class BasketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func setupCell(with basket: UserBasket) {
        idLabel.text = "\(basket.id)"
        nameLabel.text = basket.name
        priceLabel.text = "\(basket.price) руб."
        quantityLabel.text = "\(basket.quantity) шт."
    }
}
