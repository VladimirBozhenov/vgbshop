//
//  BasketModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 04/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class BasketModel {
    
    private let requestFactory = RequestFactory()
    
    func getData(userID: Int,
                 completion: @escaping (GetBasketResult?) -> ()) {
        let basket = requestFactory.makeBasketRequestFactory()
        basket.getBasket(userID: userID) { response in
            switch response.result {
            case .success(let basket):
                completion(basket)
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
}
