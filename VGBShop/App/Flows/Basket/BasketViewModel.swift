//
//  BasketViewModel.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 04/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class BasketViewModel {
    private let model = BasketModel()
    var items: [UserBasket] = []
    var ammount: Int = 0
    
    var numberOfItems: Int {
        return items.count
    }
    var itemsChanged: (() -> ())?
    
    func handleViewReady() {
        model.getData(userID: 1) { result in
            guard let result = result else { return }
            self.items = result.contents
            self.ammount = result.amount
            self.itemsChanged?()
        }
    }
    
    func productIdForIndex(_ index: Int) -> String {
        return "\(items[index].id)"
    }
    func productNameForIndex(_ index: Int) -> String {
        return items[index].name
    }
    func productPriceForIndex(_ index: Int) -> String {
        return "\(items[index].price) руб."
    }
    func productQuantityForIndex(_ index: Int) -> String {
        return "\(items[index].quantity)"
    }
    func amount() -> String {
        return "\(ammount) руб."
    }
}
