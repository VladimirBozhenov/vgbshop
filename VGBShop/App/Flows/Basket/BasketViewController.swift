//
//  BasketViewController.swift
//  VGBShop
//
//  Created by Vladimir Bozhenov on 04/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class BasketViewController: UIViewController {
    
    private let viewModel = BasketViewModel()
    
    @IBOutlet weak var basketTableView: UITableView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Корзина"
        let basketCell = UINib(nibName: "BasketTableViewCell",
                               bundle: nil)
        basketTableView.register(basketCell,
                                 forCellReuseIdentifier: "basketCell")
        
        viewModel.handleViewReady()
        viewModel.itemsChanged = { [unowned self] in
            DispatchQueue.main.async {
                self.totalPriceLabel.text = self.viewModel.amount()
                self.basketTableView.reloadData()
            }
        }
    }
}

extension BasketViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basketCell",
                                                 for: indexPath) as! BasketTableViewCell
        cell.setupCell(with: viewModel.items[indexPath.row])
        return cell
    }
}
