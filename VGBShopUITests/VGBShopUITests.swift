//
//  VGBShopUITests.swift
//  VGBShopUITests
//
//  Created by Vladimir Bozhenov on 30/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class VGBShopUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }
    
    func testAuthSuccess() {
        
        let loginField = app.textFields["Имя пользователя (Somebody)"]
        loginField.tap()
        snapshot("testScreen1")
        loginField.typeText("Somebody")
        
        let passwordField = app.textFields["Пароль (mypassword)"]
        passwordField.tap()
        snapshot("testScreen2")
        passwordField.typeText("mypassword")
        
        snapshot("testScreen3")
        app.buttons["Войти"].tap()
        
        sleep(5)
        
        snapshot("testScreen4")
        let editButton = app.buttons["Редактировать профиль"]
        let buttonValue = editButton.label
        XCTAssertTrue(buttonValue == "Редактировать профиль", "Ошибка")
    }
    
    func testAuthError() {
        
        let loginField = app.textFields["Имя пользователя (Somebody)"]
        loginField.tap()
        loginField.typeText("Somebody")
        
        let passwordField = app.textFields["Пароль (mypassword)"]
        passwordField.tap()
        passwordField.typeText("password")
        
        app.buttons["Войти"].tap()
        sleep(5)
        snapshot("testScreen5")
        checkAlert()
        
        
    }
    
    func testRegistrationSuccess() {
        
        app.buttons["Зарегистрироваться"].tap()
        
        let loginField = app.textFields["User name (Somebody)"]
        loginField.tap()
        loginField.typeText("Somebody")
        
        let passwordField = app.textFields["Password (mypassword)"]
        passwordField.tap()
        passwordField.typeText("mypassword")
        
        let emailField = app.textFields["e-mail (some@some.ru)"]
        emailField.tap()
        emailField.typeText("some@some.ru")
        
        let genderField = app.textFields["Gender (m)"]
        genderField.tap()
        genderField.typeText("m")
        
        let creditCardField = app.textFields["Credit card (9872389-2424-234224-234)"]
        creditCardField.tap()
        creditCardField.typeText("9872389-2424-234224-234")
        
        let bioField = app.textFields["Bio (This is good!)"]
        bioField.tap()
        bioField.typeText("This is good!")
        
        snapshot("testScreen6")
        app.buttons["Сохранить"].tap()
        
        sleep(5)
        
        let editButton = app.buttons["Редактировать профиль"]
        let buttonValue = editButton.label
        XCTAssertTrue(buttonValue == "Редактировать профиль", "Ошибка")
    }
    
    func checkAlert() {
        let token = addUIInterruptionMonitor(withDescription: "Неверный логин") { alert in
            alert.buttons["Ошибка"].tap()
            return true
        }
        RunLoop.current.run(until: Date(timeInterval: 5, since: Date()))
        removeUIInterruptionMonitor(token)
        
        let button = app.buttons["Войти"]
        let buttonValue = button.label
        XCTAssertTrue(buttonValue == "Войти", "Ошибка")
    }
    
    override func tearDown() {
    }
}
